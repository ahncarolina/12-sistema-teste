<?php include __DIR__ . '/../inicio-html.php'; ?>

<div class="container-fluid p-0 d-flex justify-content-between">
    <span>
        <h1><?= $titulo; ?></h1>
    </span>
    <span>
        <?php if (!$formDisabled) { ?>
            <button type="submit" form="formulario-usuario" class="btn btn-primary">Salvar</button>
        <?php } ?>
        <button class="btn btn-secondary" onclick="document.location.href='/listar-usuarios'">Cancelar</button>
    </span>

    
</div>

<form id="formulario-usuario" action="/salvar-usuario<?= isset($usuario) ? '?id=' . $usuario->getId() : ''; ?>" method="post">
    <fieldset class="form-group" <?= $formDisabled ? "disabled" : ""; ?>>
        <label for="nome">Nome</label>
        <input type="text" id="nome" name="nome" required class="form-control mb-3" value="<?= (isset($usuario) && $usuario->getNome() != null) ? $usuario->getNome() : ''; ?>">

        <label for="matricula">Matrícula</label>
        <input type="number" id="matricula" name="matricula" required class="form-control mb-3" value="<?= (isset($usuario) && $usuario->getMatricula() != null) ? $usuario->getMatricula() : ''; ?>">

        <label for="login">Login</label>
        <input type="text" id="login" name="login" required class="form-control mb-3" value="<?= (isset($usuario) && $usuario->getLogin() != null) ? $usuario->getLogin() : ''; ?>">

        <label for="senha">Senha</label>
        <input type="password" id="senha" name="senha" <?= isset($usuario) ? "" : "required" ?> class="form-control mb-3" placeholder="<?= $senhaPlaceholder; ?>">

        <label for="horario-inicio">Horário de início de trabalho</label>
        <input type="time" id="horario-inicio" name="horario-inicio" required class="form-control mb-3" value="<?= (isset($usuario) && $usuario->getHorarioInicio() != null) ? $usuario->getHorarioInicio()->format('h:i') : ''; ?>">

        <label for="horario-fim">Horário de fim de trabalho</label>
        <input type="time" id="horario-fim" name="horario-fim" required class="form-control mb-3" value="<?= (isset($usuario) && $usuario->getHorarioFim() != null) ? $usuario->getHorarioFim()->format('h:i') : ''; ?>">

        <label for="cargo">Cargo:</label>
        <select id="cargo" name="cargo" required class="form-control mb-3" >
            <option value="">Escolha uma opção</option>
            <?php 
                if ($cargos) {
                    foreach($cargos as $cargo) { 
                        $selectedCargo = false;
                        if (isset($usuario) && $usuario->getCargo() != null && $usuario->getCargo()->getId() == $cargo->getId()) {
                            $selectedCargo = true;
                        }?>
                        <option value="<?= $cargo->getId(); ?>" <?= $selectedCargo ? "selected" : ""; ?>><?= $cargo->getDescricao(); ?></option><?php
                    }
                }
            ?>
        </select>
        
        <label for="lotacao">Lotação:</label>
        <select id="lotacao" name="lotacao" required class="form-control mb-3" >
            <option value="">Escolha uma opção</option>
            <?php 
                if ($lotacoes) {
                    foreach($lotacoes as $lotacao) { 
                        $selectedLotacao = false;
                        if (isset($usuario) && $usuario->getLotacao() != null && $usuario->getLotacao()->getId() == $lotacao->getId()) {
                            $selectedLotacao = true;
                        }?>
                        <option value="<?= $lotacao->getId(); ?>" <?= $selectedLotacao ? "selected" : ""; ?>><?= $lotacao->getNome(); ?></option><?php
                    }
                }
            ?>
        </select>

        <span class="mr-2"><label>Autorizações:</label></span>
        <?php 
            if ($menus) {
                foreach($menus as $menu) { 
                    $temAutorizacao = false;
                    if (isset($idAutorizacoes) && count($idAutorizacoes) > 0 && $idAutorizacoes->contains($menu->getId())) {
                        $temAutorizacao = true;
                    }?>
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" name="autorizacoes[]" value="<?= $menu->getId(); ?>" <?= $temAutorizacao ? "checked" : ""; ?> class="custom-control-input" id="menu-<?= $menu->getId(); ?>">
                        <label class="custom-control-label" for="menu-<?= $menu->getId(); ?>"><?= $menu->getDescricao(); ?></label>
                    </div><?php
                }
            }
        ?>
    </fieldset>
</form>

<?php include __DIR__ . '/../fim-html.php'; ?>