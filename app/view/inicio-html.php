<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <title>Sistema de Gerenciamento</title>

   <!-- Bootstrap CSS CDN -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.1/font/bootstrap-icons.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
</head>
<body>

    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header" style="cursor: pointer;" onclick="document.location.href='/home'">
                <h3>Sitema de Gerenciamento</h3>
            </div>

            <ul class="list-unstyled components">
                <?php if(isset($_SESSION['autorizacao-usuario'])): ?>
                    <li>
                        <a href="#usuarioSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Usuários</a>
                        <ul class="collapse list-unstyled" id="usuarioSubmenu">
                            <li>
                                <a href="/listar-usuarios">Todos usuários</a>
                            </li>
                            <li>
                                <a href="/novo-usuario">Criar novo usuário</a>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>
                <?php if(isset($_SESSION['autorizacao-cargo'])): ?>
                    <li>
                        <a href="#cargoSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Cargos</a>
                        <ul class="collapse list-unstyled" id="cargoSubmenu">
                            <li>
                                <a href="/listar-cargos">Todos cargos</a>
                            </li>
                            <li>
                                <a href="/novo-cargo">Criar novo cargo</a>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>
                <?php if(isset($_SESSION['autorizacao-lotacao'])): ?>
                    <li>
                        <a href="#lotacaoSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Lotações</a>
                        <ul class="collapse list-unstyled" id="lotacaoSubmenu">
                            <li>
                                <a href="/listar-lotacoes">Todos lotações</a>
                            </li>
                            <li>
                                <a href="/nova-lotacao">Criar nova lotação</a>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>
            </ul>

            <div class="nav--footer">By Carol Ahn</div>
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-primary toggleBtn">
                        <i class="bi bi-x"></i>
                    </button>

                    <?php if(isset($_SESSION['logado'])): ?>
                        <a class="nav-link" href="/logout">Sair</a>
                    <?php endif; ?>
                </div>
            </nav>

            <?php if (isset($_SESSION['mensagem'])): ?>
                <div class="alert alert-<?= $_SESSION['tipo_mensagem']; ?>">
                    <?= $_SESSION['mensagem']; ?>
                </div>
            <?php
                unset($_SESSION['mensagem']);
                unset($_SESSION['tipo_mensagem']);
                endif;
            ?>


            
        
        