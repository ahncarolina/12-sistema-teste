<?php

use App\Controller\Cargo\ExcluirCargo;
use App\Controller\Cargo\FormularioAlterarCargo;
use App\Controller\Cargo\FormularioConsultarCargo;
use App\Controller\Cargo\FormularioNovoCargo;
use App\Controller\Cargo\ListarCargos;
use App\Controller\Cargo\PersistenciaCargo;
use App\Controller\Homepage;
use App\Controller\Login\FormularioLogin;
use App\Controller\Login\RealizarLogin;
use App\Controller\Login\RealizarLogout;
use App\Controller\Lotacao\ExcluirLotacao;
use App\Controller\Lotacao\FormularioAlterarLotacao;
use App\Controller\Lotacao\FormularioConsultarLotacao;
use App\Controller\Lotacao\FormularioNovaLotacao;
use App\Controller\Lotacao\ListarLotacoes;
use App\Controller\Lotacao\PersistenciaLotacao;
use App\Controller\Usuario\ExcluirUsuario;
use App\Controller\Usuario\FormularioAlterarUsuario;
use App\Controller\Usuario\FormularioConsultarUsuario;
use App\Controller\Usuario\FormularioNovoUsuario;
use App\Controller\Usuario\ListarUsuarios;
use App\Controller\Usuario\PersistenciaUsuario;

$rotas = [
    '/home' => Homepage::class,
    '/login' => FormularioLogin::class,
    '/realizar-login' => RealizarLogin::class,
    '/logout' => RealizarLogout::class,
    '/listar-usuarios' => ListarUsuarios::class,
    '/novo-usuario' => FormularioNovoUsuario::class,
    '/salvar-usuario' => PersistenciaUsuario::class,
    '/consultar-usuario' => FormularioConsultarUsuario::class,
    '/alterar-usuario' => FormularioAlterarUsuario::class,
    '/excluir-usuario' => ExcluirUsuario::class,
    '/listar-cargos' => ListarCargos::class,
    '/novo-cargo' => FormularioNovoCargo::class,
    '/salvar-cargo' => PersistenciaCargo::class,
    '/excluir-cargo' => ExcluirCargo::class,
    '/alterar-cargo' => FormularioAlterarCargo::class,
    '/consultar-cargo' => FormularioConsultarCargo::class,
    '/listar-lotacoes' => ListarLotacoes::class,
    '/excluir-lotacao' => ExcluirLotacao::class,
    '/nova-lotacao' => FormularioNovaLotacao::class,
    '/salvar-lotacao' => PersistenciaLotacao::class,
    '/alterar-lotacao' => FormularioAlterarLotacao::class,
    '/consultar-lotacao' => FormularioConsultarLotacao::class,
];

return $rotas;