<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211126194152 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE Funcionario CHANGE login login VARCHAR(255) DEFAULT NULL, CHANGE senha senha VARCHAR(255) DEFAULT NULL, CHANGE matricula matricula INT DEFAULT NULL, CHANGE horario_inicio horario_inicio TIME DEFAULT NULL, CHANGE horario_fim horario_fim TIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE Funcionario CHANGE login login VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, CHANGE senha senha VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, CHANGE matricula matricula INT NOT NULL, CHANGE horario_inicio horario_inicio TIME NOT NULL, CHANGE horario_fim horario_fim TIME NOT NULL');
    }
}
