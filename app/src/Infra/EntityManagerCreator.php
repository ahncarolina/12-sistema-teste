<?php

namespace App\Infra;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Setup;

class EntityManagerCreator
{
    public function getEntityManager(): EntityManagerInterface
    {
        $paths = [__DIR__ . '/../Entity'];
        $isDevMode = false;

        $config = Setup::createAnnotationMetadataConfiguration(
            $paths,
            $isDevMode
        );

        $connection = [
            'driver' => 'pdo_mysql',
            'host' => 'mysql57-service',
            'dbname' => 'funcionarios_db',
            'user' => 'root',
            'password' => 'secret'
        ];

        return EntityManager::create($connection, $config);
    }
}