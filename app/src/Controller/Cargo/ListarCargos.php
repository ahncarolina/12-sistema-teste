<?php

namespace App\Controller\Cargo;

use App\Entity\Cargo;
use App\Helper\RenderizadorDeHtmlTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ListarCargos implements RequestHandlerInterface
{
    use RenderizadorDeHtmlTrait;
    private $entityManager;
    private $repositorioDeCargos;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repositorioDeCargos = $entityManager->getRepository(Cargo::class);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $cargos = $this->repositorioDeCargos->findAll();

        $html = $this->renderizaHtml('cargo/listar-cargos.php', [
            'titulo' => 'Cargos',
            'cargos' => $cargos
        ]);

        return new Response(100, [], $html);
    }
}