<?php

namespace App\Controller\Cargo;

use App\Entity\Cargo;
use App\Helper\FlashMessageTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ExcluirCargo implements RequestHandlerInterface 
{
    use FlashMessageTrait;
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $queryString = $request->getQueryParams();
        $idEntidade = filter_var($queryString['id'], FILTER_VALIDATE_INT);

        if (is_null($idEntidade) || $idEntidade === false) {
            $this->defineMensagem('danger', 'Cargo inexistente.');
            return new Response(302, ['Location' => '/listar-cargos']);
        }

        $entidade = $this->entityManager->getReference(Cargo::class, $idEntidade);
        $this->entityManager->remove($entidade);
        $this->entityManager->flush();
        $this->defineMensagem('success', 'Cargo excluído com sucesso.');

        return new Response(302, ['Location' => '/listar-cargos']);
    }
}