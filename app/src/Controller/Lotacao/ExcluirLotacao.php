<?php

namespace App\Controller\Lotacao;

use App\Entity\Lotacao;
use App\Helper\FlashMessageTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ExcluirLotacao implements RequestHandlerInterface 
{
    use FlashMessageTrait;
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $queryString = $request->getQueryParams();
        $idEntidade = filter_var($queryString['id'], FILTER_VALIDATE_INT);

        if (is_null($idEntidade) || $idEntidade === false) {
            $this->defineMensagem('danger', 'Lotação inexistente.');
            return new Response(302, ['Location' => '/listar-lotacoes']);
        }

        $entidade = $this->entityManager->getReference(Lotacao::class, $idEntidade);
        $this->entityManager->remove($entidade);
        $this->entityManager->flush();
        $this->defineMensagem('success', 'Lotação excluída com sucesso.');

        return new Response(302, ['Location' => '/listar-lotacoes']);
    }
}