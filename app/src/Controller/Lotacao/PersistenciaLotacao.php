<?php

namespace App\Controller\Lotacao;

use App\Entity\Cidade;
use App\Entity\Lotacao;
use App\Helper\FlashMessageTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class PersistenciaLotacao implements RequestHandlerInterface
{
    use FlashMessageTrait;
    private $entityManager;
    private $repositorioDeLotacoes;
    private $repositorioDeCidades;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repositorioDeLotacoes = $entityManager->getRepository(Lotacao::class);
        $this->repositorioDeCidades = $entityManager->getRepository(Cidade::class);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $bodyString = $request->getParsedBody();
        $nome = filter_var($bodyString['nome'], FILTER_SANITIZE_STRING);
        $sigla = filter_var($bodyString['sigla'], FILTER_SANITIZE_STRING);
        $idCidade = filter_var($bodyString['cidade'], FILTER_SANITIZE_NUMBER_INT);

        if (!is_null($idCidade) && $idCidade !== false) {
            $cidade = $this->repositorioDeCidades->find($idCidade);
        }

        $queryString = $request->getQueryParams();
        $idEntidade = filter_var($queryString['id'], FILTER_VALIDATE_INT);

        if (!is_null($idEntidade) && $idEntidade !== false) {
            # PUT request
            $entity = $this->repositorioDeLotacoes->find($idEntidade);
            $entity->setNome($nome);
            $entity->setSigla($sigla);

            if (!is_null($cidade) && $cidade != false) {
                $entity->setCidade($cidade);
            } elseif (!is_null($entity->getCidade())) {
                $cidadeAntiga = $entity->getCidade();
                $entity->getCidade()->removeElement($cidadeAntiga);
            }

            $this->defineMensagem('success', 'Lotação alterada com sucesso.');

        } else {
            # POST request
            $lotacao = new Lotacao();
            $lotacao->setNome($nome);
            $lotacao->setSigla($sigla);

            if (!is_null($cidade) && $cidade != false) {
                $lotacao->setCidade($cidade);
            }

            $this->entityManager->persist($lotacao);
            $this->defineMensagem('success', 'Lotação criada com sucesso.');
        }

        $this->entityManager->flush();
        return new Response(200, ['Location' => '/listar-lotacoes']);
    }
}