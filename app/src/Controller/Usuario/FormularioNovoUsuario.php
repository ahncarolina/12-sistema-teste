<?php

namespace App\Controller\Usuario;

use App\Entity\Cargo;
use App\Entity\Lotacao;
use App\Entity\Menu;
use App\Helper\RenderizadorDeHtmlTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class FormularioNovoUsuario implements RequestHandlerInterface
{
    use RenderizadorDeHtmlTrait;
    private $entityManager;
    private $repositorioDeCargos;
    private $repositorioDeLotacoes;
    private $repositorioDeMenus;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repositorioDeCargos = $entityManager->getRepository(Cargo::class);
        $this->repositorioDeLotacoes = $entityManager->getRepository(Lotacao::class);
        $this->repositorioDeMenus = $entityManager->getRepository(Menu::class);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $cargos = $this->repositorioDeCargos->findAll();
        $lotacoes = $this->repositorioDeLotacoes->findAll();
        $menus = $this->repositorioDeMenus->findAll();

        $html = $this->renderizaHtml('usuario/formulario.php', [
            'titulo' => 'Novo usuário',
            'cargos' => $cargos,
            'lotacoes' => $lotacoes,
            'menus' => $menus,
            'senhaPlaceholder' => 'Nova senha'
        ]);
        return new Response(200, [], $html);
    }
}