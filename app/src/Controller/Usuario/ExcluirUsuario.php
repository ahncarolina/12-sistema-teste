<?php

namespace App\Controller\Usuario;

use App\Entity\Funcionario;
use App\Helper\FlashMessageTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ExcluirUsuario implements RequestHandlerInterface
{
    use FlashMessageTrait;
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $queryString = $request->getQueryParams();
        $idEntidade = filter_var($queryString['id'], FILTER_VALIDATE_INT);

        if (is_null($idEntidade) || $idEntidade === false) {
            $this->defineMensagem('danger', 'Usuário inexistente.');
            return new Response(302, ['Location' => '/listar-usuarios']);
        }

        $entidade = $this->entityManager->getReference(Funcionario::class, $idEntidade);
        $this->entityManager->remove($entidade);
        $this->entityManager->flush();
        $this->defineMensagem('success', 'Usuário excluído com sucesso.');

        return new Response(302, ['Location' => '/listar-usuarios']);
    }
}