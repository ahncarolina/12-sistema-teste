<?php

namespace App\Controller\Usuario;

use App\Entity\Funcionario;
use App\Helper\RenderizadorDeHtmlTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ListarUsuarios implements RequestHandlerInterface
{
    use RenderizadorDeHtmlTrait;
    private $entityManager;
    private $repositorioDeUsuarios;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repositorioDeUsuarios = $entityManager->getRepository(Funcionario::class);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $usuarios = $this->repositorioDeUsuarios->findAll();

        $html = $this->renderizaHtml('usuario/listar-usuarios.php', [
            'titulo' => 'Usuários',
            'usuarios' => $usuarios
        ]);
        return new Response(200, [], $html);
    }
}